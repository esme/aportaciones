﻿using Aportaciones.Entidades;
using Aportaciones.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Aportaciones.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NuevaCtaTesorero : ContentPage
    {
        NuevaCuentaAdminModel model;
        Repositorio<Persona> personaRepositorio;
        Repositorio<Conjunto> conjuntoRepositorio;
        public NuevaCtaTesorero()
        {
            InitializeComponent();
            model = BindingContext as NuevaCuentaAdminModel;
            personaRepositorio = new Repositorio<Persona>();
            conjuntoRepositorio = new Repositorio<Conjunto>();
        }

        private void btnCrearCuenta_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(model.Password2))
            {
                DisplayAlert("Nueva cuenta", "La contraseña no puede estar vacía", "OK");
            }
            else
            {
                if (model.Password2 == model.Persona.Password)
                {
                    if (string.IsNullOrEmpty(model.Persona.Email) || string.IsNullOrEmpty(model.Persona.Nombres) || string.IsNullOrEmpty(model.Persona.Apellidos) || string.IsNullOrEmpty(model.Conjunto.Direccion) || string.IsNullOrEmpty(model.Conjunto.Nombre))
                    {
                        DisplayAlert("Nueva cuenta", "Hay datos faltantes", "OK");
                    }
                    else
                    {
                        model.Persona.EsAdministrador = true;
                        Persona p = personaRepositorio.Create(model.Persona);
                        if (p != null)
                        {
                            model.Conjunto.IdRepresentante = p.Id;
                            Conjunto conjunto = conjuntoRepositorio.Create(model.Conjunto);
                            if (conjunto != null)
                            {
                                DisplayAlert("Éxito", "Tu cuenta ha sido creada, ya puedes iniciar sesion", "OK");
                                if (model.Persona.NumeroCasa != null)
                                {
                                    p.IdConjunto = conjunto.Id;
                                    personaRepositorio.Update(p);
                                }
                            }
                            else
                            {
                                DisplayAlert("Error", conjuntoRepositorio.Error, "OK");
                            }

                        }
                        else
                        {
                            DisplayAlert("Error", personaRepositorio.Error, "OK");
                        }
                    }
                }
                else
                {
                    DisplayAlert("Nueva cuenta", "Las contraseñas no coinciden", "OK");
                }
            }
        }
    }
}