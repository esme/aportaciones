﻿using Aportaciones.Entidades;
using Aportaciones.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Aportaciones.Views.Administrador
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuAdmin : ContentPage
    {
        Persona administrador;
        public MenuAdmin(Persona administrador)
        {
            InitializeComponent();
            this.administrador = administrador;
            BindingContext = new MenuAdminModel(administrador);
        }

        private void btnPeriodos_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Periodos((BindingContext as MenuAdminModel).Administrador.IdConjunto));
        }

        private void btnVecinos_Clicked(object sender, EventArgs e)
        {

        }

        private void btnAportaciones_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Aportaciones((BindingContext as MenuAdminModel).Administrador.IdConjunto));
        }

        private void btnGastos_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Gastos((BindingContext as MenuAdminModel).Administrador.IdConjunto));
        }

        private void btnBalance_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new BalanceGeneral((BindingContext as MenuAdminModel).Administrador.IdConjunto));
        }
    }
}