﻿using Aportaciones.Entidades;
using Aportaciones.Modelos;
using Aportaciones.Views;
using Aportaciones.Views.Administrador;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Aportaciones
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        Repositorio<Persona> repositorio;
        LoginModel model;
        public MainPage()
        {
            InitializeComponent();
            repositorio = new Repositorio<Persona>();
            model = BindingContext as LoginModel;
        }

        private void btnIniciarSesion_Clicked(object sender, EventArgs e)
        {
            Persona usuario = repositorio.Query(p => p.Email == model.Email && p.Password == model.Password).SingleOrDefault();
            if (usuario!=null)
            {
                DisplayAlert("Mis Aportaciones", "Bienvenido" + usuario.Nombres, "OK");
                if (usuario.EsAdministrador)
                {
                    Navigation.PushAsync(new MenuAdmin(usuario));
                }
                else
                {
                  
                }
            }
            else
            {
                DisplayAlert("Error", "Usuario y/o contraseña incorrecta", "OK");
            }
        }

        private void btnCrearCuenta_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new CreaCuenta());
        }
    }
}
