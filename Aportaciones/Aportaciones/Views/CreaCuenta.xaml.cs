﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Aportaciones.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreaCuenta : ContentPage
    {
        public CreaCuenta()
        {
            InitializeComponent();
        }

        private void btnCuentaTesorero_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new NuevaCtaTesorero());
        }

        private void btnCuentaVecino_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new NuevaCuentaVecino());
        }

        private void btnTerminos_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Terminos());
        }
    }
}