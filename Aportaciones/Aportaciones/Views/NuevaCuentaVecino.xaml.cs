﻿using Aportaciones.Entidades;
using Aportaciones.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Aportaciones.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NuevaCuentaVecino : ContentPage
    {
        Repositorio<Persona> repositorioPersona;
        NuevaCuentaVecinoModel model;
        public NuevaCuentaVecino()
        {
            InitializeComponent();
            repositorioPersona = new Repositorio<Persona>();
            model = BindingContext as NuevaCuentaVecinoModel;
        }

        private void btnCrearCuenta_Clicked(object sender, EventArgs e)
        {
            if (model.ConjuntoSeleccionado != null)
            {
                if (string.IsNullOrEmpty(model.Persona.Apellidos) || string.IsNullOrEmpty(model.Persona.Email) || string.IsNullOrEmpty(model.Persona.Nombres) || string.IsNullOrEmpty(model.Persona.NumeroCasa) || string.IsNullOrEmpty(model.Persona.Password)||string.IsNullOrEmpty(model.Password2))
                {
                    DisplayAlert("Nueva Cuenta", "Datos Faltantes", "OK");
                }
                else
                {
                    if (model.Password2 == model.Persona.Password)
                    {
                        model.Persona.IdConjunto = model.ConjuntoSeleccionado.Id;
                        model.Persona.EsAdministrador = false;
                        if (repositorioPersona.Create(model.Persona) != null)
                        {
                            DisplayAlert("Éxito", "Cuenta creada correctamente", "OK");
                        }
                        else
                        {
                            DisplayAlert("Error", repositorioPersona.Error, "OK");
                        }
                    }
                    else
                    {
                        DisplayAlert("Nueva Cuenta", "Las contraseñas no coinciden", "OK");
                    }
                }
            }
            else
            {
                DisplayAlert("Nueva Cuenta", "Debes seleccionar un conjunto", "OK");
            }
        }
    }
}