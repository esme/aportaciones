﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Aportaciones.Modelos
{
    public class BalanceItemModel
    {
        public DateTime Fecha { get; set; }
        public bool EsAportacion { get; set; }
        public string Concepto { get; set; }
        public float Monto { get; set; }
    }
}
