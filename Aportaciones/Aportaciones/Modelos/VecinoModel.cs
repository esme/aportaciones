﻿using Aportaciones.Entidades;
using Aportaciones.Views.Administrador;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aportaciones.Modelos
{
    public class VecinoModel
    {
        Repositorio<Persona> repositorio;
        public List<Persona> Vecinos { get; set; }
        public string IdConjunto { get; set; }
        public Periodo VecinoSeleccionado { get; set; }
        public VecinoModel()
        {

        }
        public VecinoModel(string idConjunto)
        {
            repositorio = new Repositorio<Persona>();
            Vecinos = repositorio.Query(v => v.IdConjunto == idConjunto).ToList();
            IdConjunto = idConjunto;
        }
    }
}
