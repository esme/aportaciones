﻿using Aportaciones.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aportaciones.Modelos
{
    public class MenuAdminModel
    {
        private Repositorio<Movimiento> repositorioMovimientos;
        public Persona Administrador { get; set; }
        public float Aportaciones { get;private set; }
        public float Gastos { get;private set; }
        public float Total { get;private set; }
        public MenuAdminModel()
        {

        }
        public MenuAdminModel(Persona persona)
        {
            Administrador = persona;
            repositorioMovimientos = new Repositorio<Movimiento>();
            Aportaciones = repositorioMovimientos.Query(m => m.IdConjunto == Administrador.IdConjunto && m.EsEntrada).Sum(n => n.Monto);
            Gastos= repositorioMovimientos.Query(m => m.IdConjunto == Administrador.IdConjunto &&!m.EsEntrada).Sum(n => n.Monto);
            Total = Aportaciones - Gastos;
        }
    }
}
