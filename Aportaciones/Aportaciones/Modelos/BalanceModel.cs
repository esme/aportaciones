﻿using Aportaciones.Entidades;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aportaciones.Modelos
{
    public class BalanceModel
    {
        Repositorio<Persona> repositorioPersonas;
        Repositorio<Movimiento> repositorio;
        List<Movimiento> TodosLosMovimientos;
        Repositorio<Periodo> repositorioPeriodos;
        string idConjunto;
        public List<BalanceItemModel> Movimientos { get; set; }
        public Movimiento MovimientoSeleccionado { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fin { get; set; }
        public float TotalActual { get; set; }
        public DateTime FechaActual { get; set; }
        public PlotModel GraficoModel { get; set; }
        public BalanceModel()
        {

        }
        public BalanceModel(string idConjunto)
        {
            repositorio = new Repositorio<Movimiento>();
            repositorioPeriodos = new Repositorio<Periodo>();
            repositorioPersonas = new Repositorio<Persona>();
            Inicio = DateTime.Now;
            Fin = DateTime.Now;
            TodosLosMovimientos = repositorio.Query(m => m.IdConjunto == idConjunto).ToList();
            TotalActual = TodosLosMovimientos.Where(a => a.EsEntrada).Sum(p => p.Monto) - TodosLosMovimientos.Where(a => !a.EsEntrada).Sum(p => p.Monto);
            FechaActual = DateTime.Now;
            this.idConjunto = idConjunto;
        }
        public void BuscarPorFecha()
        {
            List<BalanceItemModel> datos = new List<BalanceItemModel>();
            foreach (var item in TodosLosMovimientos.Where(m => m.Pago >= Inicio && m.Pago <= Fin))
            {
                datos.Add(new BalanceItemModel()
                {
                    Concepto = item.EsEntrada ? "Casa" + repositorio.SearchById(item.IdPersona).NumCasa : item.Notas,
                    EsAportacion = item.EsEntrada,
                    Fecha = item.FechaHora,
                    Monto = item.Monto
                });
            }
            List<PuntoGraficoModel> datosGrafico = new List<PuntoGraficoModel>();
            foreach (var periodo in repositorioPeriodos.Query(p => p.IdConjunto == idConjunto).OrderBy(f => f.FechaHora))
            {
                var movimientos = TodosLosMovimientos.Where(m => m.IdPeriodo == periodo.Id);
                datosGrafico.Add(new PuntoGraficoModel()
                {
                    Periodo = periodo.Nombre,
                    Monto = movimientos.Where(n => n.EsEntrada).Sum(j => j.Monto) - movimientos.Where(k => !k.EsEntrada).Sum(l => l.Monto)
                });
            }
            GraficoModel = new PlotModel();
            Axis ejeX = new CategoryAxis()
            {
                Position = AxisPosition.Bottom,
                ItemsSource = datosGrafico.Select(d => d.Periodo)
            };
            Axis ejeY = new LinearAxis()
            {
                Position = AxisPosition.Left,
            };
            GraficoModel.Axes.Add(ejeX);
            GraficoModel.Axes.Add(ejeY);
            LineSeries linea = new LineSeries();
            linea.ItemsSource = datosGrafico.Select(d => d.Monto);
            GraficoModel.Series.Add(linea);
            linea.Title = "Aportaciones";
            GraficoModel.Title = "Balance general";
        }
    }
}
