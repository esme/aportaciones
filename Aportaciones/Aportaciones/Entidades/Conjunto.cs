﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Aportaciones.Entidades
{
    public class Conjunto:BaseDTO
    {
        public string Direccion { get; set; }
        public string IdRepresentante { get; set; }
        public string Nombre { get; set; }
        public override string ToString()
        {
            return Nombre;
        }
    }
}
